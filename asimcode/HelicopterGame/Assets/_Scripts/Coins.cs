﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour {
	public GameObject meter;

	// Use this for initialization
	void Start () {
	
	}
	bool check=false;
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.transform.tag == "sub") {
			check = true;

		}
		if (coll.transform.tag == "magnet") {
			check = true;

		}

	}

	// Update is called once per frame
	void Update () {
		if (check == true) {
			transform.position = Vector2.Lerp (transform.position, meter.transform.position, 2.8f * Time.deltaTime);
		}

	}
}
