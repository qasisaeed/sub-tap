﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;
using Facebook.Unity;
public class FacebookManager : MonoBehaviour
{
	
	/// <summary>
	/// User's facebook name.
	/// </summary>
	string facebookName;

	/// <summary>
	/// User's facebook picture URL.
	/// </summary>
	string facebookPictureUrl;


	public delegate void ScoreDelegate(List<object> scoreList);

	public ScoreDelegate sDelegate; 

	/// <summary>
	/// Function used to initialize facebook.
	/// </summary>
	public void Initialize()
	{
		

		if (!FB.IsLoggedIn)
		{
			FB.Init(OnInitCompleted, OnHideUnity);
		}
		else
		{
			//this.OnFacebookLoginCompletedCallback(FB.AccessToken);
//			FB.API("me", Facebook.Unity.HttpMethod.GET, OnProfileInfoLoaded);
			GetScores();
		}
	}

	/// <summary>
	/// Callback used when Facebook initialization is complete.
	/// </summary>
	void OnInitCompleted()
	{
		if (!FB.IsLoggedIn)
		{
			
			FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.OnLoginCompleted);
		}
		else
		{
			LoginWithPublishPermissions ();
			//this.OnFacebookLoginCompletedCallback(FB.AccessToken);
		}
	}

	/// <summary>
	/// Callback used when Facebook login is complete.
	/// </summary>
	/// <param name="result">Login result</param>
	void OnLoginCompleted(ILoginResult result)
	{
		if (result.Error != null)
		{
			Debug.Log("error facebook " + result.Error);
		}
		else if (!FB.IsLoggedIn)
		{
			Debug.Log("login cancelled, by user or error");
		}
		else
		{
			Debug.Log("login successful");
			Invoke ("LoginWithPublishPermissions", 2.0f);
			//			FB.API("me", Facebook.Unity.HttpMethod.GET, OnProfileInfoLoaded);
		}
	}

	void LoginWithPublishPermissions()
	{
		 
//			Debug.Log("login successful");
		FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, this.OnPublishLoginCompleted);
			//			FB.API("me", Facebook.Unity.HttpMethod.GET, OnProfileInfoLoaded);
		 
	}

	void OnPublishLoginCompleted(ILoginResult result)
	{
		if (result.Error != null)
		{
			Debug.Log("error facebook " + result.Error);
		}
		else if (!FB.IsLoggedIn)
		{
			Debug.Log("login cancelled, by user or error");
		}
		else
		{
			Debug.Log("Successfull Permission Aquired !");
			GetScores ();
			//			FB.API("me", Facebook.Unity.HttpMethod.GET, OnProfileInfoLoaded);
		}
	}
	/// <summary>
	/// Callback used when user's profile info loads.
	/// </summary>
	/// <param name="result"></param>
	void OnProfileInfoLoaded(IGraphResult result)
	{
		Dictionary<string, object> profileInfo = (Dictionary<string, object>)Json.Deserialize(result.RawResult);

		facebookName = profileInfo["name"].ToString();

		FB.API("me/picture?width=100&height=100&redirect=false", Facebook.Unity.HttpMethod.GET, OnPictureLoaded);
	}

	/// <summary>
	/// Callback used when user's facebook picture loads.
	/// </summary>
	/// <param name="result"></param>
	void OnPictureLoaded(IGraphResult result)
	{
		Dictionary<string, object> receivedInfo = (Dictionary<string, object>)Json.Deserialize(result.RawResult);
		Dictionary<string, object> pictureInfo = (Dictionary<string, object>)receivedInfo["data"];

		facebookPictureUrl = pictureInfo["url"].ToString();


	}

	/// <summary>
	/// Function needed to handle application win/lose focus
	/// </summary>
	/// <param name="isGameShown">Is focus on app?</param>
	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			// pause the game - we will need to hide
			Time.timeScale = 0;
		}
		else
		{
			// start the game back up - we're getting focus again
			Time.timeScale = 1;
		}
	}

	public void GetScores ()
	{
		FB.API("/app/scores?fields=score,user.limit(20)", HttpMethod.GET, GetScoresCallback);
	}

	private void GetScoresCallback(IGraphResult result) 
	{
		Debug.Log("GetScoresCallback");
		if (result.Error != null)
		{
			Debug.LogError(result.Error);
			return;
		}
		Debug.Log(result.RawResult);

		// Parse scores info
		var scoresList = new List<object>();

		object scoresh;
		if (result.ResultDictionary.TryGetValue ("data", out scoresh)) 
		{
			scoresList = (List<object>) scoresh;
		}
		sDelegate (scoresList);
		// Parse score data
//		HandleScoresData (scoresList);


	}


//	private void HandleScoresData (List<object> scoresResponse)
//	{
//		var structuredScores = new List<object>();
//		foreach(object scoreItem in scoresResponse) 
//		{
//			// Score JSON format
//			// {
//			//   "score": 4,
//			//   "user": {
//			//      "name": "Chris Lewis",
//			//      "id": "10152646005463795"
//			//   }
//			// }
//
//			Dictionary<string,object> entry = (Dictionary<string,object>) scoreItem;
//			var user = (Dictionary<string,object>) entry["user"];
//			string userName = (string)user["name"];
//			int score = Convert.ToInt32(entry["score"]);
//
////			if (string.Equals(userId, AccessToken.CurrentAccessToken.UserId))
////			{
////				
////			}
//
//			structuredScores.Add(entry);
//
//		}
//			
//	}

	// The Graph API for Scores allows you to publish scores from your game to Facebook
	// This allows Friend Smash! to create a friends leaderboard keeping track of the top scores achieved by the player and their friends.
	// For more information on the Scores API see: https://developers.facebook.com/docs/games/scores
	//
	// When publishing a player's scores, these scores will be visible to their friends who also play your game.
	// As a result, the player needs to grant the app an extra permission, publish_actions, in order to publish scores.
	// This means we need to ask for the extra permission, as well as handling the
	// scenario where that permission wasn't previously granted.
	//
	public void PostScore (int score, Action callback = null)
	{
		// Check for 'publish_actions' as the Scores API requires it for submitting scores
//		if (FB.IsLoggedIn)
//		{
			var query = new Dictionary<string, string>();
			query["score"] = score.ToString();
			FB.API(
				"/me/scores",
				HttpMethod.POST,
				delegate(IGraphResult result)
				{
					Debug.Log("PostScore Result: " + result.RawResult);
					// Fetch fresh scores to update UI
					GetScores();
				},
				query
			);
//		}
//		else
//		{
			// Showing context before prompting for publish actions
			// See Facebook Login Best Practices: https://developers.facebook.com/docs/facebook-login/best-practices
//			PopupScript.SetPopup("Prompting for Publish Permissions for Scores API", 4f, delegate
//				{
//					// Prompt for `publish actions` and if granted, post score
//					FBLogin.PromptForPublish(delegate
//						{
//							if (FBLogin.HavePublishActions)
//							{
//								PostScore(score);
//							}
//						});
//				});
//		}
	}

	/// <summary>
	/// Facebook Logout
	/// </summary>
	public void Logout()
	{
		FB.LogOut();
	}
}