﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class UIScript : MonoBehaviour {
	public GameObject GamePlay;
	public GameObject canvasUI;
	public GameObject btn1;
	public GameObject btn2;
	public GameObject btn3;
	public GameObject pausebtn;
	//public GameObject SoundOffBtn;
	//public GameObject network;
	public GameObject pauseDialog;
	public GameObject gameplaypanel;
	public Text life;
	public Text dailycoins;
	public GameObject dailybonus;
	public GameObject leaderboard;
	public FacebookManager fbManager;
	public GameObject gameOver;
	public Text[] Scores;
	public Text[] Name;

	Dictionary<string,string> friendsScore = new Dictionary<string, string>();
	// Use this for initialization
	void Start () {
		
		if (PlayerPrefs.GetInt("bonus") ==0){
			//Debug.Log( System.DateTime.Now.Day);
			PlayerPrefs.SetInt("bonus", System.DateTime.Now.Day);
		}
		if (PlayerPrefs.GetInt("bonus") != System.DateTime.Now.Day) {
			dailybonus.SetActive (true);
			int coin =  (UnityEngine.Random.Range (100, 200));
			dailycoins.text = coin.ToString ();
			PlayerPrefs.SetInt ("coin", PlayerPrefs.GetInt("coin")+coin);


			PlayerPrefs.SetInt("bonus", System.DateTime.Now.Day);
		}

		if (PlayerPrefs.GetInt("lifes") == 0){
		PlayerPrefs.SetInt ("lifes", 5);
		}

		Debug.Log(System.DateTime.Now.Hour);


		Time.timeScale = 1;
		if (PlayerPrefs.GetInt ("Replay") == 1) {
			canvasUI.SetActive (false);
			gameplaypanel.SetActive (true);
			GamePlay.SetActive (true);
			PlayerPrefs.SetInt ("Replay", 0);
		}

		fbManager.sDelegate += PopulateLeaderBoard;
	
	}


	public void PopulateLeaderBoard(List<object> scoresResponse ){
//		leaderboard.SetActive(true);
		//		fbManager.GetScores();
	foreach(object scoreItem in scoresResponse) 
	{
		// Score JSON format
		// {
		//   "score": 4,
		//   "user": {
		//      "name": "Chris Lewis",
		//      "id": "10152646005463795"
		//   }
		// }

			Dictionary<string,object> entry = (Dictionary<string,object>) scoreItem;
			var user = (Dictionary<string,object>) entry["user"];
			string userName = (string)user["id"];
			string scoreValue = entry["score"].ToString();
			friendsScore.Add (userName, scoreValue);
			Debug.Log ("userName:"+userName+" scoreValue:"+scoreValue);
		//			if (string.Equals(userId, AccessToken.CurrentAccessToken.UserId))
		//			{
		//				
		//			}

		//			structuredScores.Add(entry);

	}
	}
	int counter=0;
	public void leaderboardBtnClicked(){
		gameOver.SetActive (false);
		foreach (KeyValuePair<string,string> item in friendsScore){
			Scores[counter].text = item.Key; // Name
			Name[counter].text = item.Value; //Score
			counter++;
		}
		leaderboard.SetActive(true);
//		fbManager.GetScores();
}

	public void DailyButtonOk (){
		dailybonus.SetActive (false);
	}
	public void playBtn(){
		gameplaypanel.SetActive (true);
		canvasUI.SetActive (false);
		GamePlay.SetActive (true);
	}
	int hit=0;
	public void networkingbtn(){
		if (hit == 0) {
			//network.SetActive (true);
			btn1.SetActive (true);
			btn2.SetActive (true);
			btn3.SetActive (true);
			hit += 1;
		} else if (hit == 1) {
			btn1.SetActive (false);
			btn2.SetActive (false);
			btn3.SetActive (false);
			hit -= 1;
		}
	}
	public void LeaderBoardCross(){
		leaderboard.SetActive (false);
		gameOver.SetActive (true);
	}
	public void facebookbtn(){
//		Application.OpenURL ("www.facebook.com");
		fbManager.Initialize();

	}
	public void pause(){
		Time.timeScale = 0;
		pauseDialog.SetActive (true);
		pausebtn.SetActive (false);
	}
	public void resume(){
		Time.timeScale = 1;
		pauseDialog.SetActive (false);
		pausebtn.SetActive (true);
	}
	public void mainmenu(){

		Application.LoadLevel ("GamePlayScene");
	}
	void ReplayTime(){
		Debug.Log (System.DateTime.Now);
	}
	public void Restart(){
		
		//canvasUI.SetActive (false);
		ReplayTime();
		GamePlay.SetActive (false);
		GamePlay.SetActive (true);
	}
	public void Replay(){
//		if (PlayerPrefs.GetInt ("lifes") == 1){
//			Debug.Log ("no Life");
//		}
//		else{
		PlayerPrefs.SetInt ("lifes", PlayerPrefs.GetInt("lifes")-1);
		PlayerPrefs.SetInt("minute",System.DateTime.Now.Minute);
		PlayerPrefs.SetInt("hours",System.DateTime.Now.Hour);
		PlayerPrefs.SetInt ("Replay", 1);
		Application.LoadLevel ("GamePlayScene");
		//}
	}
	public void moreapp(){
		Application.OpenURL ("https://play.google.com/store?hl=en");
	}

	// Update is called once per frame
	void Update () {

//		int i = PlayerPrefs.GetInt ("lifes") - 1;
//		life.text =i.ToString();
//		if (PlayerPrefs.GetInt ("minute")+1 <= System.DateTime.Now.Minute) {
//			PlayerPrefs.SetInt ("minute", PlayerPrefs.GetInt ("minute") + 1);
//		}
	}
}
