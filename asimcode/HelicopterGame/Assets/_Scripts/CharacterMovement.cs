﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CharacterMovement : MonoBehaviour {
	public Image bar;
	public Image distancebar;
	private float maxtime= 120f;
	private float mintime=0f;
	private float distancetime= 120f;
	private float distancemintime=0f;
	public float speed = 2;
	private Animator blast;
	public float force = 100;
	public Text coin;
	Rigidbody2D _rigidBody;
	public GameObject gameOver;
	float counters;
	public GameObject spark;
	public GameObject fuelcol;
	Animator animator;
	public GameObject pause;
	public AudioClip impact;
	public AudioClip coinsound;
	AudioSource audio;
	public GameObject bubble;
	private bool shieldCheck = false;
	public GameObject magnet;
	public AudioClip chestSound;
	public AudioClip fuelSound;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		animator = GetComponent<Animator>();
		mintime = maxtime;
		InvokeRepeating ("decreasetime", 0f, 0.3f);
		InvokeRepeating ("distancetimes",0f, 1f);
		counters = 20.0f;
		Time.timeScale = 1;

		_rigidBody = GetComponent<Rigidbody2D>();
		_rigidBody.velocity = Vector2.right * speed;
	}
	void distancetimes()
	{
		
		distancemintime += 1f;

		float calctime = distancemintime/distancetime;
		distime (calctime);
	}
	void distime(float distimes){
		distancebar.fillAmount = distimes;

		if (distancemintime == 120f) {
		Time.timeScale = 0;
		}
	}
	void decreasetime(){
		mintime -= 0.5f;
		float calctime = mintime / maxtime;
		Settime (calctime);
	}
	void Settime(float timeup)
	{
		bar.fillAmount = timeup;
		if (timeup < 0) {
			Time.timeScale = 0;
			gameOver.SetActive (true);
		}
	}
	// Update is called once per frame
	string coun;

	void Update () {
		

		//		counters -= Time.deltaTime;
//		bar.fillAmount = counters;
//		if (counters < 0) {
//			Time.timeScale = 0;
//			gameOver.SetActive (true);
//		}
		coun = PlayerPrefs.GetInt ("coin").ToString ();
		coin.text = coun;
		// Upward Force
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown(0)){
//			_rigidBody.velocity = Vector2.zero;
			_rigidBody.AddForce(Vector2.up * force);
		}

	}
	void blastAni (){
		
		Time.timeScale = 0;
		Destroy (gameObject);

		gameOver.SetActive (true);
		pause.SetActive (false);			
	}
	void blastrocks(){
		
			Time.timeScale = 0;
		Destroy (gameObject);

		gameOver.SetActive (true);
		pause.SetActive (false);

	}
		int counter;
	void OnCollisionEnter2D(Collision2D col){
		if (shieldCheck == false) {
			if (col.transform.tag == "mines") {
				animator.SetBool ("minesblast", true);
				audio.PlayOneShot (impact, 8F);
				Invoke ("blastAni", 0.6f);

			}

			if (col.transform.tag == "rocks") {
				animator.SetBool ("PlayerDead", true);
				audio.PlayOneShot (impact, 8F);
				Invoke ("blastrocks", 0.6f);

			}

			if (col.transform.tag == "Missiles") {
				animator.SetBool ("minesblast", true);
				audio.PlayOneShot (impact, 8F);
				Invoke ("blastAni", 0.6f);

			}
		}

	}
	void buble(){
		bubble.SetActive (false);
		shieldCheck = false;
	}
	void Sheild(){
		
		bubble.SetActive (true);
		shieldCheck = true;
		Invoke ("buble", 10);
	}
	void flarepark(){
		fuelcol.SetActive (false);
	}
	void OnTriggerEnter(Collider coll){
		Debug.Log ("Col"+ coll.name);

	}
	void coinss(){
		spark.SetActive(false);

	}
	void magnetTime(){
		magnet.SetActive (false);
	}
	void OnTriggerEnter2D(Collider2D coll){
		
		if (coll.transform.tag == "coin") {
			spark.SetActive (true);
			audio.PlayOneShot (coinsound, 1.5f);
			counter = PlayerPrefs.GetInt ("coin") +1;
			PlayerPrefs.SetInt ("coin", counter);
			Invoke ("coinss", 1f);
		}
		if (coll.transform.tag == "fuel") {
			fuelcol.SetActive (true);
			//float i = maxtime % mintime;
			//mintime = mintime + i;
			audio.PlayOneShot (fuelSound, 3F);
			mintime = 120;
			Invoke ("flarepark", 0.6f);
			Destroy (coll.gameObject);
		}
		if (coll.transform.tag == "gift") {
			audio.PlayOneShot (chestSound, 3F);
			spark.SetActive (true);
			Destroy (coll.gameObject);
			Invoke ("coinss", 1f);
		}
		if (coll.transform.tag == "Bubble") {
			audio.PlayOneShot (fuelSound, 3F);
			Destroy (coll.gameObject);
			Sheild ();
		}
		if (coll.transform.tag == "magnet") {
			audio.PlayOneShot (fuelSound, 3F);
			magnet.SetActive (true);
			Destroy (coll.gameObject);
			Invoke ("magnetTime", 8f);

		}

	}

}
