﻿using UnityEngine;
using System.Collections;

public class hurdlepos : MonoBehaviour {
	public float speed;
	public GameObject blast;
	// Use this for initialization
	void Start () {
	
	}
	void OnCollisionEnter2D(Collision2D col){
		if (col.transform.tag == "sub") {
			blast.SetActive (true);
			Destroy (gameObject,0.1f);
		}
	}

	// Update is called once per frame
	void Update () {
		transform.Translate(Vector2.left * speed * Time.deltaTime);
	}
}
