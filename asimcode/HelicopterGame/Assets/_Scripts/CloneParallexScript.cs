﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Add this class to an object so it'll move in parallax based on the level's speed.
/// Note that this method duplicates the object
/// </summary>
public class CloneParallexScript : MonoBehaviour 
{
	public static CloneParallexScript instance;
	/// the relative speed of the object
	public float ParallaxSpeed=0;
	public float speed;
	protected GameObject _clone;
	protected Vector3 _movement;
	protected Vector3 _initialPosition;
	protected float _width;
	public Transform parentObj;
	/// <summary>
	/// On start, we store various variables and clone the object
	/// </summary>
	/// 
	 void Awake()
	{
		instance = this;
	}
	void Start()
	{
			CallOnStart();
		
	}
	public void CallOnStart()
	{
		_width = GetComponent<Renderer>().bounds.size.x;
		_initialPosition=transform.position	;	
		// we clone the object and position the clone at the end of the initial object
		_clone = (GameObject)Instantiate(gameObject, new Vector3(transform.position.x+_width, transform.position.y, transform.position.z), transform.rotation);
		// we remove the parallax component from the clone to prevent an infinite loop
		CloneParallexScript parallaxComponent = _clone.GetComponent<CloneParallexScript>();
		_clone.transform.SetParent (parentObj);
		Destroy(parallaxComponent);	
//		if (bgcontroller.instance.GamePlayedOnce) {
//			for (int i = 0; i < bgcontroller.instance.clones.Length; i++) {
//				if (bgcontroller.instance.clones [i] == null) {
//					bgcontroller.instance.clones [i] = _clone;
//					return;
//
//				}
//			}
//		}





	}

	/// <summary>
	/// On FixedUpdate, we move the object and its clone
	/// </summary>
	protected virtual void Update()
	{
//		if (PlayerClass.instance.isDead == false || Time.timeScale!=0) {
			// we determine the movement the object and its clone need to apply, based on their speed and the level's speed
			//if (LevelManager.Instance != null) { 
			//_movement = Vector3.left * (ParallaxSpeed / 10) * speed * Time.fixedDeltaTime;
			//} else {
//		if (Application.platform == RuntimePlatform.WindowsEditor) {
//			_movement = Vector3.left * (ParallaxSpeed / 10) * speed * Time.fixedDeltaTime;
//		
//		}
//		else
			_movement = Vector3.left * (ParallaxSpeed / 10) * speed * Time.deltaTime;

			//}
			// we move both objects
			_clone.transform.Translate (_movement);
			transform.Translate (_movement);

			// if the object has reached its left limit, we teleport both objects to the right
			if (transform.position.x + _width < _initialPosition.x) {
				transform.Translate (Vector3.right * _width);
				_clone.transform.Translate (Vector3.right * _width);
			}
//		}
	}


}