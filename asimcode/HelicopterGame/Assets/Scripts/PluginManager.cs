﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using ChartboostSDK;


public class PluginManager : MonoBehaviour {

	public GoogleAnalyticsV4 googleAnalytics;
	public BannerView bannerView;
	public InterstitialAd interstitial;
	string zoneid = null;
	void Awake()
	{
		RequestBanner ();
		
	}
	// Use this for initialization
	void Start () {
		RequestInterstitial ();
//		UnityAdsHelper.Initialize ();
		if (!Chartboost.hasInterstitial (CBLocation.Default)) {
			Chartboost.cacheInterstitial (CBLocation.Default);
		}
		
		if (!Chartboost.hasInterstitial (CBLocation.GameScreen)) {
			Chartboost.cacheInterstitial (CBLocation.GameScreen);
		}
		
		if (!Chartboost.hasInterstitial (CBLocation.HomeScreen)) {
			Chartboost.cacheInterstitial (CBLocation.HomeScreen);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	public void RequestBanner()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-7220895901828564/5251675937";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7220895901828564/7068678738";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
		googleAnalytics.LogScreen ("BannerAd");
		
		Invoke ("showBanner", 2f);
		
	}
	
	public void RequestInterstitial()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-7220895901828564/6728409134";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7220895901828564/1022145133";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
		googleAnalytics.LogScreen ("InterstialAd");
		Invoke ("showInterstitial", 5f);
		
	}
	
	void showInterstitial ()
	{
		if (interstitial.IsLoaded ()) {
			interstitial.Show ();
		} else
			Debug.Log ("Add not shown");
	}
	
	void hideBanner()
	{
		bannerView.Hide ();
	}
	
	void showBanner(){
		bannerView.Show ();
	}
	public void ShowUnityads()
	{
//		UnityAdsHelper.ShowAd (zoneid);
		googleAnalytics.LogScreen ("UnityAd");
		
	}
	public void ShowChartboostAd()
	{
		googleAnalytics.LogScreen ("CBAd");
		
		if (Chartboost.hasInterstitial (CBLocation.Default)) {
			Chartboost.showInterstitial(CBLocation.Default);
		}
	}
	void OnEnable ()
	{
		SetupDelegates ();
		
	}
	
	void OnDisable ()
	{
		Chartboost.didFailToLoadInterstitial -= didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial -= didDismissInterstitial;
		Chartboost.didCloseInterstitial -= didCloseInterstitial;
		Chartboost.didClickInterstitial -= didClickInterstitial;
		Chartboost.didCacheInterstitial -= didCacheInterstitial;
		Chartboost.didDisplayInterstitial -= didDisplayInterstitial;
		Chartboost.willDisplayVideo -= willDisplayVideo;
		
		
		Chartboost.didFailToLoadRewardedVideo -= didFailToLoadRewardedVideo;
		Chartboost.didDismissRewardedVideo -= didDismissRewardedVideo;
		Chartboost.didCloseRewardedVideo -= didCloseRewardedVideo;
		Chartboost.didClickRewardedVideo -= didClickRewardedVideo;
		Chartboost.didCacheRewardedVideo -= didCacheRewardedVideo;
		Chartboost.shouldDisplayRewardedVideo -= shouldDisplayRewardedVideo;
		Chartboost.didCompleteRewardedVideo -= didCompleteRewardedVideo;
		Chartboost.didDisplayRewardedVideo -= didDisplayRewardedVideo;
		
	}
	
	void SetupDelegates ()
	{
		// Listen to all impression-related events
		Chartboost.didFailToLoadInterstitial += didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial += didDismissInterstitial;
		Chartboost.didCloseInterstitial += didCloseInterstitial;
		Chartboost.didClickInterstitial += didClickInterstitial;
		Chartboost.didCacheInterstitial += didCacheInterstitial;
		Chartboost.didDisplayInterstitial += didDisplayInterstitial;
		Chartboost.willDisplayVideo += willDisplayVideo;
		
		
		Chartboost.didFailToLoadRewardedVideo += didFailToLoadRewardedVideo;
		Chartboost.didDismissRewardedVideo += didDismissRewardedVideo;
		Chartboost.didCloseRewardedVideo += didCloseRewardedVideo;
		Chartboost.didClickRewardedVideo += didClickRewardedVideo;
		Chartboost.didCacheRewardedVideo += didCacheRewardedVideo;
		Chartboost.shouldDisplayRewardedVideo += shouldDisplayRewardedVideo;
		Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
		Chartboost.didDisplayRewardedVideo += didDisplayRewardedVideo;
	}
	
	void didFailToLoadInterstitial (CBLocation location, CBImpressionError error)
	{
		Debug.Log (string.Format ("didFailToLoadInterstitial: {0} at location {1}", error, location));
	}
	
	void didDismissInterstitial (CBLocation location)
	{
		Debug.Log ("didDismissInterstitial: " + location);
	}
	
	void didCloseInterstitial (CBLocation location)
	{
		Debug.Log ("didCloseInterstitial: " + location);
	}
	
	void didClickInterstitial (CBLocation location)
	{
		Debug.Log ("didClickInterstitial: " + location);
	}
	
	void didCacheInterstitial (CBLocation location)
	{
		Debug.Log ("didCacheInterstitial: " + location);
	}
	
	void didDisplayInterstitial (CBLocation location)
	{
		Debug.Log ("didDisplayInterstitial: " + location);
		
		// Hello write here 
		if (location == CBLocation.GameScreen) {
			
		}
	}
	
	void didFailToLoadRewardedVideo (CBLocation location, CBImpressionError error)
	{
		Debug.Log (string.Format ("didFailToLoadRewardedVideo: {0} at location {1}", error, location));
	}
	
	void didDismissRewardedVideo (CBLocation location)
	{
		Debug.Log ("didDismissRewardedVideo: " + location);
	}
	
	void didCloseRewardedVideo (CBLocation location)
	{
		Debug.Log ("didCloseRewardedVideo: " + location);
		
	}
	
	void didClickRewardedVideo (CBLocation location)
	{
		Debug.Log ("didClickRewardedVideo: " + location);
	}
	
	void didCacheRewardedVideo (CBLocation location)
	{
		Debug.Log ("didCacheRewardedVideo: " + location);
	}
	
	bool shouldDisplayRewardedVideo (CBLocation location)
	{
		Debug.Log ("shouldDisplayRewardedVideo: " + location);
		return true;
	}
	
	void didCompleteRewardedVideo (CBLocation location, int reward)
	{
		Debug.Log (string.Format ("didCompleteRewardedVideo: reward {0} at location {1}", reward, location));
		
	}
	
	void didDisplayRewardedVideo (CBLocation location)
	{
		Debug.Log ("didDisplayRewardedVideo: " + location);
	}
	
	void willDisplayVideo (CBLocation location)
	{
		Debug.Log ("willDisplayVideo: " + location);
	}
	
}

