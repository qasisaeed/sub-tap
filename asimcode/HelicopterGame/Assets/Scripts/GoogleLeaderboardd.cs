﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class GoogleLeaderboardd : MonoBehaviour {
	
//	public string leaderboardID; // leader board id from developer console;
	public static GoogleLeaderboardd _instance = null;
	
	// Use this for initialization
	void Awake () {



		if (_instance == null) {
			_instance = this;
			GameObject.DontDestroyOnLoad (this.gameObject);
		}
		else{
			GameObject.Destroy(this);
		}
		
	}
	// Use this for initialization
	public void Start () {
		
		if (!Social.localUser.authenticated) { // THIS WILL SIGN IN THE USER
			
			// Authenticate
			GooglePlayGames.PlayGamesPlatform.Activate(); // THIS WILL INITIALIZE GOOGLE PLAY SERVICES
			
			Social.localUser.Authenticate((bool success) => {
				if (success) {
					Debug.Log ("User Authenticated");
				}  else {
					Debug.Log ("Authentication failed.");
				}
			} );
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
//	public void postMyHighScore(int HighScore) // PUT THIS ON BUTTON TO POST HIGH SCORE
//	{
//		//Debug.Log ("Post my high score");
//		if(Social.localUser.authenticated)
//		{
//			Social.ReportScore(HighScore, leaderboardID, ((bool success) =>{
//				if(success)
//				{
//					Debug.Log("Score posted Succesfully");
//				}
//				else
//				{
//					Debug.Log("Failed to post score");
//				}
//			} )
//			                   );
//		}
//	}
//	
	public void showLeaderboardUI() // THIS WILL SHOW THE LEADERBOARD
	{
		Debug.Log ("Please show the leader board");
		//googleAnalytics.LogScreen ("Leader Board Clicked");
		//googleAnalytics.LogScreen(new AppViewHitBuilder().SetScreenName("Leader Board Clicked"));
		if(Social.localUser.authenticated)
		{
			PlayGamesPlatform.Instance.ShowLeaderboardUI (GPGSIds.leaderboard_combinations);
		}
	}
}
