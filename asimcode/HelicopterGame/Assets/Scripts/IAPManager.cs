﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Soomla.Store.Example
{
	public class IAPManager : MonoBehaviour
	{
		public static IAPManager instance = null;

		void Awake ()
		{
			if (instance == null) { 	//making sure we only initialize one instance.
				instance = this;
				GameObject.DontDestroyOnLoad (this.gameObject);
			} else {					//Destroying unused instances.
				GameObject.Destroy (this.gameObject);
			}

		}
		void Start ()
		{
			StoreEvents.OnSoomlaStoreInitialized += OnSoomlaStoreInitialized;
			StoreEvents.OnItemPurchased += OnItemPurchased;
			StoreEvents.OnItemPurchaseStarted += OnItemPurchaseStarted;
			StoreEvents.OnMarketPurchase += OnMarketPurchase;
			StoreEvents.OnMarketPurchaseCancelled += OnMarketPurchaseCancelled;
			StoreEvents.OnMarketPurchaseStarted += OnMarketPurchaseStarted;
			StoreEvents.OnRestoreTransactionsStarted += OnRestoreTransactionsStarted;
			StoreEvents.OnRestoreTransactionsFinished += OnRestoreTransactionsFinished;
			StoreEvents.OnGoodBalanceChanged += onGoodBalanceChanged;


			SoomlaStore.Initialize (new SoomlaTestStore ());

		}
		public void buyNoAds ()
		{
			StoreInventory.BuyItem ("remove_ads");

		}
		public void AddTimeAndExtendGame ()
		{
			StoreInventory.BuyItem ("add_time");

			
		}

		public void RestoreAds ()
		{
			int amount = StoreInventory.GetItemBalance ("remove_ads");
			if (amount > 0)
				PlayerPrefs.SetInt ("showAds", 0);
		}

		public void buyCoinAndGem (int charNum)
		{
			Debug.Log ("Buy Character No: " + charNum);
			if (charNum == 1)
				StoreInventory.BuyItem ("add_time");
			else
				Debug.Log ("Invalid Number");
			Debug.Log ("After Soomla Buy Item");
		}

		public void OnSoomlaStoreInitialized ()
		{
			Debug.Log ("OnSoomlaStoreInitialized IAP Manager Event");
			SoomlaStore.RestoreTransactions ();
			int itemBalance = StoreInventory.GetItemBalance ("remove_ads");
			if (itemBalance == 1) {
				PlayerPrefs.SetInt ("showAds", 0);
				Debug.Log ("Ads has been removed using OnSoomlaStoreInitialized");
			}
		}

		void OnItemPurchased (PurchasableVirtualItem item, string payload)
		{
			Debug.Log ("OnItemPurchased IAP Manager Event");
		}
		void OnItemPurchaseStarted (PurchasableVirtualItem item)
		{
			Debug.Log ("OnItemPurchaseStarted IAP Manager Event");			
		}
		void OnMarketPurchaseStarted (PurchasableVirtualItem item)
		{
			Debug.Log ("OnMarketPurchaseStarted IAP Manager Event");
			int itemBalance = StoreInventory.GetItemBalance ("remove_ads");
			if (itemBalance == 1) {
				PlayerPrefs.SetInt ("showAds", 0);
				Debug.Log ("Ads has been removed using OnMarketPurchaseStarted");
			}
		}
		void OnMarketPurchaseCancelled (PurchasableVirtualItem item)
		{
			Debug.Log ("OnMarketPurchaseCancelled IAP Manager Event");
		}
		void OnMarketPurchase (PurchasableVirtualItem item, string payload, Dictionary<string, string> extra)
		{

			if (item.ItemId.Equals ("remove_ads")) {
				PlayerPrefs.SetInt ("showAds", 0);
				Debug.Log ("Ads has been removed");
			} else if (item.ItemId.Equals ("add_time")) {

				Debug.Log("Do SomeThing Here");
//				Timer.timer.ExtendGame ();
//				int temp = PlayerPrefs.GetInt ("coins");
//				temp = temp + 15000;
//				PlayerPrefs.SetInt("coins",temp);
			}
		}
		void OnRestoreTransactionsStarted ()
		{
			Debug.Log ("Restore Transactions Started");
		}
		void OnRestoreTransactionsFinished (bool success)
		{
			SoomlaStore.RefreshMarketItemsDetails ();
			int itemBalance = StoreInventory.GetItemBalance ("remove_ads");
			if (itemBalance == 1) {
				PlayerPrefs.SetInt ("showAds", 0);
				Debug.Log ("Ads has been removed using OnRestoreTransactionsFinished");
			}
		}
		public void onGoodBalanceChanged (VirtualGood good, int balance, int amountAdded)
		{

		}
		public void OnUnexpectedErrorInStore (string message)
		{
			Debug.Log ("OnUnexpectedErrorInStore: " + message);
		}
	}
}