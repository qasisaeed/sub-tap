﻿namespace Facebook.Unity.Example
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

public class FacebookSharing : MonoBehaviour {

		void Awake()
		{
			FB.Init();
			
		}
		// Use this for initialization
		void Start () {

			LoginFB ();
		}
		
		// Update is called once per frame
		void Update () {
		
		}


		public void LoginFB()
		{
			if (FB.IsInitialized) {

					FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" });
				
			}
		}
		public void FBShare()
		{
			FB.FeedShare(
				string.Empty,
				new Uri("https://developers.facebook.com/"),
				"Test Title",
				"Test caption",
				"Test Description",
				new Uri("http://i.imgur.com/zkYlB.jpg"),
				string.Empty);
		}
	}
}
